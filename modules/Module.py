#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 15-May-2022

# This module contains the classes that produce the XML file for uploading
# the Module calibration data.

from BaseUploader import BaseUploader, ConditionData
from lxml         import etree
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files
from Exceptions   import *
from enum         import Enum
from datetime     import datetime

import json,os

HybridSide  = Enum('HybridSide' ,'Right, Left')
CBCPosition = Enum('CBCPosition','0X01, 0X02, 0X03, 0X04, 0X05, 0X06, 0X07, 0X08')
DataType    = Enum('DataType','CalibrationSummary, SCurveHybrid, SCurveCBC')
ModuleType  = Enum('ModuleType','2S, PS')
BlockType   = Enum('BlockType','Noise, Pedestal, Offset, Occupancy')

class ModuleCalibrationT(ConditionData):
   """Produces the XML file for storing the Module Calibration data."""
   data_description  = {('2S','CalibrationSummary'):  {  
                            'kind_of_part' : '2S Module',     
                            'cond_name'    : 'Module Scurve Summary',
                            'table_name'   : 'SCURVE_SMMRY',
                      'DBvar_vs_TxtHeader' : {'ROOT_FILE'     : '',
                                              'ROOT_DATE'     : '',
                                              'NOISE_AVG'     : '',
                                              'NOISE_RMS'     : '',
                                             },
                               'mandatory' : []
                                                       },
                        ('2S','SCurveHybrid'):  {  
                            'kind_of_part' : '2S Module',     
                            'cond_name'    : 'Module Scurve Hybrid Data',
                            'table_name'   : 'SCURVE_HYBRID',
                      'DBvar_vs_TxtHeader' : {'HYBRID_SERIAL'    : '',
                                              'HYBRID_SIDE'      : '',
                                              'NOISE_AVG'        : '',
                                              'NOISE_RMS'        : '',
                                              'NOISE_NOUTL_LOW'  : '',
                                              'NOISE_OUTL_LOW'   : '',
                                              'NOISE_NOUTL_HIGH' : '',
                                              'NOISE_OUTL_HIGH'  : '', 
                                             },
                               'mandatory' : ['HYBRID_SERIAL','HYBRID_SIDE']
                                                 },
                        ('2S','SCurveCBC'):  {  
                            'kind_of_part' : '2S Module',     
                            'cond_name'    : 'Module Scurve CBC Data',
                            'table_name'   : 'SCURVE_CBC',
                      'DBvar_vs_TxtHeader' : {'HYBRID_SERIAL'        : '',
                                              'HYBRID_SIDE'          : '',
                                              'CBC_SERIAL'           : '',
                                              'CBC_POS_HYB'          : '',
                                              'NOISE_AVG'            : '',
                                              'NOISE_RMS'            : '',
                                              'NOISE_NOUTL_LOW'      : '',
                                              'NOISE_OUTL_LOW'       : '',
                                              'NOISE_NOUTL_HIGH'     : '',
                                              'NOISE_OUTL_HIGH'      : '',
                                              'PEDESTAL_AVG'         : '',
                                              'PEDESTAL_RMS'         : '',
                                              'PEDESTAL_NOUTL_LOW'   : '',
                                              'PEDESTAL_OUTL_LOW'    : '',
                                              'PEDESTAL_NOUTL_HIGH'  : '',
                                              'PEDESTAL_OUTL_HIGH'   : '',
                                              'OFFSET_AVG'           : '',
                                              'OFFSET_RMS'           : '',
                                              'OFFSET_NOUTL_LOW'     : '',
                                              'OFFSET_OUTL_LOW'      : '',
                                              'OFFSET_NOUTL_HIGH'    : '',
                                              'OFFSET_OUTL_HIGH'     : '',
                                              'OCCUPANCY_AVG'        : '',
                                              'OCCUPANCY_RMS'        : '',
                                              'OCCUPANCY_NOUTL_LOW'  : '',
                                              'OCCUPANCY_OUTL_LOW'   : '',
                                              'OCCUPANCY_NOUTL_HIGH' : '',
                                              'OCCUPANCY_OUTL_HIGH'  : '',
                                              'VPLUS'                : '',
                                             },
                               'mandatory' : ['HYBRID_SERIAL','HYBRID_SIDE',
                                              'CBC_SERIAL','CBC_POS_HYB']
                                                 },
                        }   

   def __init__(self, serial, data_type, data_version='v1', inserter=None):
      """Constructor: it requires the serial number and the type of data (data_type). It also
         accepts the data set version (set to 'v1' as default) and the inserter name which is
         not used by default.

         Mandatory template configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional template parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the data (override the CERN user name)
      """

      # Check the module type
      try:
         ModuleType[str(serial.split('_')[0])]  # check if type of module is recognized
      except:
         print(Fore.RED+f'{serial}'+Style.RESET_ALL+' is not recognized as Module serial number!')
         exit(1)

      self.serial = serial
      self.h_kind = str(serial.split('_')[0])

      # Check the type of data
      try:
         DataType[str(data_type)]  # check if type of data is recognized
      except:
         types = [el.name for el in DataType]
         print(Fore.RED+f'{data_type}'+Style.RESET_ALL+' is not a valid data type!')
         print('valid data types are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
         exit(1)
         
      self.h_data = data_type    # stores the type of test

      name = f'{serial}_calibration'

      configuration = {}
      data_description = self.data_description[(self.h_kind,self.h_data)]
      configuration.update(deepcopy(data_description))
      configuration['serial'] = serial
      configuration['data_version']=data_version
      
      ConditionData.__init__(self, name, configuration, None)


   def build_data_block(self,dataset):
      """Builds the data block."""
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      
      if(not isinstance(data_description,list)):
         data_description = [data_description]

      for record in data_description:
         data = etree.SubElement(dataset,"DATA")
         # Check for mandatory tags
         for el in self.retrieve("mandatory"):
            if record[el]=='':
               raise MissingParameter(self.__class__.__name__,el)

         for el in record.keys():
            if record[el]!='':
               etree.SubElement(data,f"{el}").text = record[el]


   def load_run_metadata(self, run_type=None, run_number=None, run_begin=None, run_end=None, 
                               run_operator=None, run_location=None, run_comment=None ):
      """Loads the run metadata."""
      if run_type!=None: self.update_configuration('run_type',run_type)
      if run_number!=None: self.update_configuration('run_number',run_number)
      if run_begin!=None: self.update_configuration('run_begin',run_begin.strftime('%Y-%m-%d %H:%M:%S'))
      if run_end!=None: self.update_configuration('run_end',run_end.strftime('%Y-%m-%d %H:%M:%S'))
      if run_operator!=None: self.update_configuration('run_operator',run_operator)
      if run_location!=None: self.update_configuration('run_location',run_location)
      if run_comment!=None: self.update_configuration('run_comment',run_comment)


   def load_block(self,block_type, mean, std, nOutliersLow=0, outliersLow=None,
                       nOutliersHigh=0,outliersHigh=None, vplus=None):
      """Loads the data block."""
      # Check the type of data
      try:
         BlockType[str(block_type)]  # check if type of block is recognized
      except:
         types = [el.name for el in BlockType]
         print(Fore.RED+f'{block_type}'+Style.RESET_ALL+' is not a valid block type!')
         print('valid block types are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
         exit(1)

      name = block_type.upper()
      self.update_data_block(f'{name}_AVG',str(mean))
      self.update_data_block(f'{name}_RMS',str(std))
      if self.h_data!='CalibrationSummary':
         if nOutliersLow!=None:  self.update_data_block(f'{name}_NOUTL_LOW',str(nOutliersLow))
         if outliersLow!=None:   self.update_data_block(f'{name}_OUTL_LOW',str(outliersLow))
         if nOutliersHigh!=None: self.update_data_block(f'{name}_NOUTL_HIGH',str(nOutliersHigh))
         if outliersHigh!=None:  self.update_data_block(f'{name}_OUTL_HIGH',str(outliersHigh))
         if vplus!=None:  self.update_data_block('VPLUS',str(vplus))

   def load_hybrid_id(self, serial, side):
      """Loads the hybrid id."""
      # Check the side description
      try:
         HybridSide[str(side)]  # check if type of block is recognized
      except:
         types = [el.name for el in HybridSide]
         print(Fore.RED+f'{side}'+Style.RESET_ALL+' is not a valid block type!')
         print('valid block types are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
         exit(1)
      
      self.update_data_block('HYBRID_SERIAL',str(serial)) 
      self.update_data_block('HYBRID_SIDE',str(side))
      
   
   def load_cbc_id(self, serial, position):
      """Loads the hybrid id."""
      # Check the side description
      try:
         CBCPosition[str(position)]  # check if type of block is recognized
      except:
         types = [el.name for el in CBCPosition]
         print(Fore.RED+f'{position}'+Style.RESET_ALL+' is not a valid block type!')
         print('valid block types are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
         exit(1)
      
      self.update_data_block('CBC_SERIAL',str(serial)) 
      self.update_data_block('CBC_POS_HYB',str(position))


   def load_root_file(self,data_path):
      """Load the root file."""
      files  = search_files(data_path,"*.root")
      if files[0]!='':
         from shutil import copyfile
         source = os.path.join(files[0])
         target = os.path.join(os.getcwd(),f'{self.name}.root')
         copyfile(source,target)
      
         data_block = self.retrieve('DBvar_vs_TxtHeader')
         filename   = f'{self.name}.root'
         filetime   = os.path.getmtime(filename)
         from datetime import datetime
         dt = datetime.fromtimestamp(filetime)
         data_block['ROOT_FILE'] = filename
         data_block['ROOT_DATE'] = dt.strftime('%Y-%m-%d %H:%M:%S')


   def update_data_block(self,name,value):
      """Update the data block content. Check the consistency of the name tag."""
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      data_block_tags = data_block.keys()
      if name not in data_block_tags:
         raise BadParameters(self.__class__.__name__,f'{name} is not a tag for {self.h_kind} {self.h_test}.')
      data_block[name]=value
      
      
   def dump_xml_data(self, filename=''):
      """Dump the sensor condition data in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = f'{self.name}.xml'

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      
      from zipfile import ZipFile
      import os
      rootfilename = self.retrieve('DBvar_vs_TxtHeader')['ROOT_FILE']
      zipfilename  = f'{self.name}.zip'
      to_be_zipped = [f'{self.name}.xml']
      if rootfilename!='': to_be_zipped.append(rootfilename)
      with ZipFile(zipfilename, 'w') as (zip):
         for f in to_be_zipped: zip.write(f)
         for f in to_be_zipped: os.remove(f)
      
      return zipfilename
  
   def merge_with_root(self):
      from zipfile import ZipFile
      import os
      rootfilename = self.retrieve('DBvar_vs_TxtHeader')['ROOT_FILE']
      zipfilename  = f'{self.name}.zip'
      to_be_zipped = [f'{self.name}.xml']
      if rootfilename!='': to_be_zipped.append(rootfilename)
      with ZipFile(zipfilename, 'w') as (zip):
         for f in to_be_zipped: zip.write(f)
         for f in to_be_zipped: os.remove(f)
      
      return zipfilename